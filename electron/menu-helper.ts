import {app, BrowserWindow, MenuItemConstructorOptions, Menu} from 'electron';

export class MenuHelper {

  constructor(private mainWindow: BrowserWindow) {
  }

  private mainMenuTemplate: MenuItemConstructorOptions[] = [
    {
      label: 'File',
      submenu: [
        {
          label: 'Exit',
          role: 'close'
        }
      ]
    },
    {
      label: 'View',
      submenu: [
        {role: 'reload'},
        {role: 'forcereload'},
        {role: 'toggledevtools'},
        {type: 'separator'},
        {role: 'resetzoom'},
        {role: 'zoomin'},
        {role: 'zoomout'},
        {type: 'separator'},
        {role: 'togglefullscreen'}
      ]
    },
    {
      label: 'Help',
      submenu: [
        {
          label: 'About',
          click: () => {
            this.mainWindow.webContents.send('about');
          }
        }
      ]
    }
  ];

  private trayMenuTemplate: MenuItemConstructorOptions[] = [
    {
      label: 'New screenshot',
      click: () => {
        this.mainWindow.webContents.send('screenshot');
      }
    },
    {
      label: 'Exit',
      click: () => { app.quit(); }
    }
  ];

  get mainMenu() {
    return Menu.buildFromTemplate(this.mainMenuTemplate);
  }

  get trayMenu() {
    return Menu.buildFromTemplate(this.trayMenuTemplate);
  }
}
