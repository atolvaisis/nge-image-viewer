import { NgModule } from '@angular/core';
import {MatButtonModule, MatListModule, MatIconModule, MatCardModule, MatDialogModule} from '@angular/material';

const materialModules = [
  MatButtonModule,
  MatListModule,
  MatIconModule,
  MatCardModule,
  MatDialogModule
];

@NgModule({
  imports: [
    ...materialModules
  ],
  exports: [
    ...materialModules
  ],
  declarations: []
})
export class MaterialModule { }
