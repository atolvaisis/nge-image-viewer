import {ChangeDetectorRef, Component} from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import {NavigationService} from './services/navigation.service';
import {ElectronMenuService} from './services/electron-menu.service';
import {LocalStorageService} from './services/local-storage.service';
import {FileInfo} from './models/file-info';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  currentPath = '';
  // currentPath$ = new BehaviorSubject<string>(this.currentPath);

  constructor(private changeDetector: ChangeDetectorRef, private navigationService: NavigationService,
              private localStorageService: LocalStorageService,
              private electronMenuService: ElectronMenuService) { }

  ngOnInit() {
  }

  currentPathChanged(fileInfo: FileInfo) {
    this.currentPath =  fileInfo == null ? '' : fileInfo.fullPath;
    // this.currentPath$.next(this.currentPath);
    this.navigationService.currentPathInfo$.next(fileInfo);
    this.localStorageService.setCurrentPath(this.currentPath);
    this.changeDetector.detectChanges();
    // console.log(path);
  }

}
