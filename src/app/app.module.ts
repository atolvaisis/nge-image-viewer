import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MaterialModule } from './modules/material.module';
import { NativeService } from './services/native.service';
import { FileExplorerComponent } from './components/file-explorer/file-explorer.component';
import { FileService } from './services/file.service';
import { GalleryComponent } from './components/gallery/gallery.component';
import { PathComponent } from './components/path/path.component';
import {NavigationService} from './services/navigation.service';
import {ElectronMenuService} from './services/electron-menu.service';
import { AboutComponent } from './components/about/about.component';
import {LocalStorageService} from './services/local-storage.service';
import {ScreenshotService} from './services/screenshot.service';


@NgModule({
  declarations: [
    AppComponent,
    FileExplorerComponent,
    GalleryComponent,
    PathComponent,
    AboutComponent
  ],
  entryComponents: [
    AboutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [
    NativeService,
    FileService,
    NavigationService,
    ElectronMenuService,
    LocalStorageService,
    ScreenshotService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
