import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {FileInfo} from '../models/file-info';

@Injectable()
export class NavigationService {

  currentPathInfo$: BehaviorSubject<FileInfo>;

  constructor() {
    this.currentPathInfo$ = new BehaviorSubject<FileInfo>(null);
    /*ngZone.run(() => {
      const storedPath = localStorageService.getCurrentPath();

      let currentDirectory = null;
      if (storedPath != null && fileService.pathExists(storedPath)) {
        const pathInfo = fileService.parsePath(storedPath);
        console.log(pathInfo);

        currentDirectory = new FileInfo();
        currentDirectory.name = pathInfo.name;
        currentDirectory.fullPath = storedPath;
        currentDirectory.isDirectory = true;
      }

      this.currentPathInfo$ = new BehaviorSubject<FileInfo>(currentDirectory == null ? null : currentDirectory);
    });*/
  }

}
