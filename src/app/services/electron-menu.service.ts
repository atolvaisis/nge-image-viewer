import {Injectable, NgZone} from '@angular/core';
import {NativeService} from './native.service';
import {MatDialog} from '@angular/material';
import {AboutComponent} from '../components/about/about.component';
import {ScreenshotService} from './screenshot.service';

@Injectable()
export class ElectronMenuService {

  constructor(private nativeService: NativeService, private dialog: MatDialog, private ngZone: NgZone,
              private screenshotService: ScreenshotService) {
    nativeService.electron.ipcRenderer.on('about', () => {
      this.showAboutDialog();
    });

    nativeService.electron.ipcRenderer.on('screenshot', () => {
      this.screenshotService.takeScreenshot();
    });
  }

  showAboutDialog() {
    this.ngZone.run(() => {
      this.dialog.closeAll();
      this.dialog.open(AboutComponent);
    });
  }

}
