import {Injectable} from '@angular/core';
import {NativeService} from './native.service';

@Injectable()
export class LocalStorageService {

  readonly imageViewerCurrentPathKey = 'imageViewerCurrentPath';

  constructor(private nativeService: NativeService) {
  }

  setItem(key: string, value: string) {
    this.nativeService.nativeWindow.localStorage.setItem(key, value);
  }

  getItem(key: string) {
    return this.nativeService.nativeWindow.localStorage.getItem(key);
  }

  setCurrentPath(path: string) {
    this.setItem(this.imageViewerCurrentPathKey, path);
  }

  getCurrentPath(): string {
    return this.getItem(this.imageViewerCurrentPathKey);
  }

}
