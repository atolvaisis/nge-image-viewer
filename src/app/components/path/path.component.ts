import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NavigationService} from '../../services/navigation.service';
import {FileInfo} from '../../models/file-info';

@Component({
  selector: 'app-path',
  templateUrl: './path.component.html',
  styleUrls: ['./path.component.scss']
})
export class PathComponent implements OnInit {

  currentPathInfo: FileInfo;

  constructor(private navigationService: NavigationService, private changeDetector: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.navigationService.currentPathInfo$.subscribe(f => {
      this.currentPathInfo = f;
      this.changeDetector.detectChanges();
    });
  }

}
