import {ChangeDetectorRef, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FileOptions, FileService} from '../../services/file.service';
import { FileInfo } from '../../models/file-info';
import {LocalStorageService} from '../../services/local-storage.service';

@Component({
  selector: 'app-file-explorer',
  templateUrl: './file-explorer.component.html',
  styleUrls: ['./file-explorer.component.scss']
})
export class FileExplorerComponent implements OnInit {

  @Output()
  currentPathChanged: EventEmitter<FileInfo> = new EventEmitter<FileInfo>();

  directories: FileInfo[];

  constructor(private fileService: FileService,
              private localStorageService: LocalStorageService,
              private cd: ChangeDetectorRef) { }

  ngOnInit() {
    const storedPath = this.localStorageService.getCurrentPath();

    const path = storedPath != null && this.fileService.pathExists(storedPath) ? storedPath :  this.fileService.getHomeDirectoryPath();

    const fileInfo = new FileInfo();
    fileInfo.fullPath = this.fileService.normalizePath(path);
    fileInfo.name = fileInfo.fullPath;
    this.readDirectoryInfo(fileInfo);
  }

  readDirectoryInfo(fileInfo: FileInfo) {
    const upDirectory = this.fileService.getUpDirectory(fileInfo.fullPath);
    this.fileService.getFilesWithStat(fileInfo.fullPath, FileOptions.DirectoriesOnly).subscribe(f => {
      const directories = [];
      if (upDirectory.fullPath !== fileInfo.fullPath) {
        directories.push(upDirectory);
      }
      directories.push(...f);
      this.directories = directories;
      this.cd.detectChanges(); // workaround
      this.currentPathChanged.emit(fileInfo);
    });
  }

  directoryClicked(fileInfo: FileInfo) {
    console.log(fileInfo);
    this.readDirectoryInfo(fileInfo);
  }

}
