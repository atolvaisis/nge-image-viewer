import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private dialog: MatDialogRef<AboutComponent>) { }

  ngOnInit() {
  }

  close() {
    // console.log('close');
    this.dialog.close();
    // this.dialog.afterClosed().subscribe(() => {
    //   console.log('after closed');
    // });
  }

}
